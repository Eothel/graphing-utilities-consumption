/**
 * Checks if a given value is a number.
 * @param value The value to check if it's a number.
 * @returns {boolean} true if the value is a number.
 */
export let isNumber = value => typeof value === "number";

/**
 * Checks if a given value is null.
 * @param value The value to check for null.
 * @returns true if the value is null.
 */
export let isNull = value => value === null;

export let isDefined = value => {
    return !isNull(value) && !isUndefined(value);
};

/**
 * Checks if a given value is undefined.
 * @param value The value to check for undefined.
 * @returns true if the value is undefined.
 */
export let isUndefined = value => value === undefined;

/**
 * Checks if a given value is an array.
 * This is an alias for the standard <code>Array.isArray()</code>.
 */
export let isArray = Array.isArray;

/**
 * Checks if a given value is a function
 * @param value The value that should be a function
 * @returns {boolean} True if the value is indeed a function
 */
export let isFunction = value => {
    return typeof value === 'function';
};

/**
 * Checks if a given value is a string
 * @param value The value that should be a string
 * @returns {boolean} True if the value is ineed a string
 */
export let isString = value => typeof value === 'string';

/**
 * Throws an error if the value is null or undefined.
 * @param value The value to check.
 * @param name The value's label.
 * @returns {*} The value passes through on success.
 */
export let required = (value, name) => {
    if(isNull(value) || isUndefined(value)) {
        throw new Error(`Parameter '${name}' must be neither null nor undefined`);
    }
    return value;
};

/**
 * Throws an error if the value is not an array.
 * @param value The value to check.
 * @param name The value's label.
 * @returns {*} The value passes through on success.
 */
export let requiredArray = (value, name) => {
    required(value, name);
    if(!Array.isArray(value)) {
        throw new Error(`Parameter '${name}' must be of type Array`);
    }
    return value;
};

export let requiredFunction = (value, name) => {
    required(value, name);
    if(!isFunction(value)) {
        throw new Error(`Parameter ${name} must be a function`);
    }
};

export let requiredString = (value, name) => {
    required(value, name);
    if(!isString(value)) {
        throw new Error(`Parameter ${name} must be a string`);
    }
    return value;
};

/**
 * Throws an error if the value is not of a given type.
 * TODO: Only works for named constructor functions. How to make this more reliable?
 * @param value The value to check.
 * @param type The type constructor that should be matched. E.g. Date, RegExp, Promise, ...)
 * @param name The value's label.
 * @returns {*} The value passes through on success.
 */
export let requiredType = (value, type, name) => {
    required(value, name);
    if(Object.getPrototypeOf(value) !== type.prototype) {
        throw new Error(`Parameter '${name}' must be of type ${type.name}`);
    }
    return value;
};

/**
 * Throws an error if the value is not a number.
 * @param value The value to check.
 * @param name The value's label.
 * @returns {*} The value passes through on success.
 */
export let requiredNumber = (value, name) => {
    required(value, name);
    if(!isNumber(value)) {
        throw new Error(`Parameter '${name}' must be of type number`);
    }
    return value;
};

/**
 * Rounds a number to precision amount of decimal places.
 * @param value The value to be rounded.
 * @param precision The amount of decimal places to round to
 * @returns {number} The rounded number.
 */
export let round = (value, precision) => {
    requiredNumber(value, 'value');
    requiredNumber(precision, 'precision');

    var order = Math.pow(10, precision);
    return Math.round(value * order) / order;
};