import moment from 'moment';
import {extent, min, max} from 'd3';

let loadData = (url) => {
    return new Promise((resolve, reject) => {
        let xhr = new XMLHttpRequest();
        xhr.open('get', url);
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    try {
                        let data = JSON.parse(xhr.responseText)
                            .sort((a, b) => a > b ? 1 : -1);
                        resolve(data);
                    } catch(e) {
                        reject(new Error(`Error during XHR: ${e}`));
                    }
                } else {
                    reject(new Error(`Error during XHR: ${xhr.status} ${xhr.statusMessage}`));
                }
            }
        };
        xhr.send('get', url);
    });
};

let isoMoment = isoString => {
    return moment(isoString, 'YYYY-MM-DD');
};

let calculateDomains = (data) => {
    let valueDomain = extent(data, d => d.average);
    valueDomain[0] = 0; // TODO: Currently value domain always starts at zero (will be problem if negatives are involved)

    return {
        time: [min(data, d => d.first), max(data, d => d.last)],
        value: valueDomain
    }
};

export let series = (name, unit, url, method, accessor = d => d.value) => {
    let self = {name, unit};

    self.days = () => {
        let days = [];
        for(let datum of self.data) {
            let date = datum.first.clone();
            while(date.isSameOrBefore(datum.last)) {
                days.push({
                    first: date.clone(),
                    last: date.clone(),
                    average: datum.average,
                    duration: 1,
                });
                date.add(1, 'days');
            }
        }
        return days;
    };

    self.group = lastDayOfGroupBoundaryFunction => {
        let sum = 0, count = 0, first = null;
        let weeks = [], days = self.days();
        for(let i = 0; i < days.length; i++) {
            let day = days[i];
            if(first == null) {
                first = day.first.clone();
            }
            sum += day.average;
            count++;
            if(lastDayOfGroupBoundaryFunction(day.first) || i == days.length - 1) {
                let week = {
                    first,
                    last: day.first.clone(),
                    duration: count,
                    average: sum / count
                };
                weeks.push(week);
                sum = count = 0;    // Reset values for next iteration
                first = null;
            }
        }
        return weeks;
    };

    self.weeks = () => {
        return self.group(d => d.isoWeekday() === 7); // Groups from monday to sunday
    };

    self.months = () => {
        return self.group(d => d.date() === d.clone().endOf('month').date());
    };

    self.years = () => {
        return self.group(d => d.date() === 31 && d.month() === 11); // December = 11
    };

    return loadData(url).then(data => {
        self.data = method(data, accessor);
        self.domains = calculateDomains(self.data);
        return self;
    });
};

export let vertex = (data, accessor) => {
    let vertices = [];
    for(let datum of data) {
        let vertex = {};
        vertex.first = isoMoment(datum.date);    // Parses both day and month (first of month) patterns
        if(/^(\d{4})-(\d{2})-(\d{2})$/.test(datum.date)) {  // Day pattern: 'YYYY-MM-DD'
            vertex.last = vertex.first.clone();
            vertex.duration = 1;
        } else if(/^(\d{4})-(\d{2})$/.test(datum.date)) {   // Month pattern: 'YYYY-MM'
            vertex.last = vertex.first.clone().endOf('month');
            vertex.duration = vertex.first.clone().add('1', 'month').diff(vertex.first, 'days');
        } else {
            throw new Error(`Invalid date pattern '${datum.date}' must be either 'YYYY-MM' or 'YYYY-MM-DD'`);
        }
        vertex.average = accessor(datum);
        vertices.push(vertex);
    }
    return vertices;
};

export let cumulativeEdge = (data, accessor) => {
     data = data.map(d => {
        return {
            date: d.date,
            value: accessor(d)
        };
    });

    let edges = [];
    let previous = data[0];
    for(let i = 1; i < data.length; i++) {
        let current = data[i];

        let value = current.value;
        if(value === null) {
            // In case there is a null entry (yes, it happens!)
            continue;
        }
        value -= previous.value; // Decumulate

        // Date ranges currently assume the day of the measurement is the first day of the new range
        // this it assumes measurement is taken in the beginning of the day (before major consumption has taken place).
        // Further refinement would require time (hour) of measurement to be included.
        let edge = {};
        let startOfNext = isoMoment(current.date);
        edge.first = isoMoment(previous.date);
        edge.last = startOfNext.clone().subtract(1, 'days');
        edge.duration = startOfNext.diff(edge.first, 'days');
        edge.average = value / edge.duration;
        edges.push(edge);
        previous = current;
    }
    return edges;
};