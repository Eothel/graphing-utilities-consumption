import * as d3 from 'd3';
import * as utils from './utils.js';

export class MultiGraph {
    constructor(leftSeries, rightSeries) {
        this.leftSeries = utils.required(leftSeries);
        this.rightSeries = rightSeries;

        this.timeDomain = this.calculateTimeDomain(this.leftSeries, this.rightSeries);
    }

    calculateTimeDomain(leftSeries, rightSeries) {
        let dates = [].concat(leftSeries.domains.time);
        if(rightSeries) {
            dates = dates.concat(rightSeries.domains.time);
        }
        return d3.extent(dates);
    }

    draw(targetElement, dayWidth = 2, group = null) {
        let duration = this.timeDomain[1].diff(this.timeDomain[0], 'days');

        let padding = 30;
        let outerSize = {
            width: duration * dayWidth,
            height: 400,
        };

        let innerSize = {
            width: outerSize.width - 2 * padding,
            height: outerSize.height - 2 * padding
        };

        let svgElement = targetElement.append('svg');
        svgElement.attr('width', outerSize.width);
        svgElement.attr('height', outerSize.height);

        let contentGroup = svgElement.append('g');
        contentGroup.attr('transform', `translate(${padding}, ${padding})`);

        let {scales, axes, domains} = this.generateScalesAndAxes(innerSize);
        contentGroup.node().appendChild(this.drawAxes(axes, innerSize));
        contentGroup.node().appendChild(this.drawData(scales, domains, group));
    }

    generateScalesAndAxes(size) {
        let leftAndRightScaleAccessor = d => d.average;
        let result = {
            axes: {},
            scales: {},
            domains: {},
        };

        // Left scale
        let leftDomain = this.leftSeries.domains.value.reverse();
        let leftScale = d3.scaleLinear().domain(leftDomain).rangeRound([0, size.height]);
        let leftAxis = d3.axisLeft(leftScale);
        result.axes.left = leftAxis;
        result.scales.left = leftScale;
        result.domains.left = leftDomain;

        // Right scale
        if(utils.isDefined(this.rightSeries)) {
            let rightDomain = this.rightSeries.domains.value.reverse();
            let rightScale = d3.scaleLinear().domain(rightDomain).rangeRound([0, size.height]);
            let rightAxis = d3.axisRight(rightScale);
            result.axes.right = rightAxis;
            result.scales.right = rightScale;
            result.domains.right = rightDomain;
        }

        // Bottom scale
        let bottomDomain = this.timeDomain;
        let bottomScale = d3.scaleTime().domain(bottomDomain).rangeRound([0, size.width]);
        let bottomAxis = d3.axisBottom(bottomScale);
        bottomAxis.ticks(d3.timeMonth.every(1));
        bottomAxis.tickFormat(d3.timeFormat('%Y-%m'));
        result.axes.bottom = bottomAxis;
        result.scales.bottom = bottomScale;
        result.domains.bottom = bottomDomain;

        return result;
    }

    drawAxes(axes, size) {
        let axesGroup = d3.select(document.createElementNS(d3.namespaces.svg, 'g'));
        axesGroup.append('g').attr('transform', `translate(0, 0)`).call(axes.left);
        if(utils.isDefined(axes.right)) {
            axesGroup.append('g').attr('transform', `translate(${size.width}, 0)`).call(axes.right);
        }
        axesGroup.append('g').attr('transform', `translate(0, ${size.height})`).call(axes.bottom);
        return axesGroup.node();
    }

    drawData(scales, domains, group) {
        let dataGroup = d3.select(document.createElementNS(d3.namespaces.svg, 'g'));

        // Left data
        let leftXFunction = e => scales.bottom(e.first);
        let leftWidthFunction = e => scales.bottom(e.last.clone().endOf('day')) - scales.bottom(e.first);
        var leftYFunction = e => scales.left(e.average);
        var leftData = group === null ? this.leftSeries.data : this.leftSeries[group]();
        let leftDatum = dataGroup.selectAll('rect').data(leftData).enter().append('g');

        function color(normalizedValue) {
            let hue = Math.round(-120 * normalizedValue + 120);
            return `hsl(${hue}, 100%, 50%)`;
        }
        let maxValue = Math.max.apply(Math, domains.left);

        leftDatum.append('rect')
            .attr('x', leftXFunction)
            .attr('y', leftYFunction)
            .attr('width', leftWidthFunction)
            .attr('height', e => {
                return scales.left(0) - scales.left(e.average);
            })
            .attr('fill', e => color(e.average / maxValue))
            .attr('stroke', e => color(e.average / maxValue));
        leftDatum.append('text')
            .attr("x", e => leftXFunction(e) + leftWidthFunction(e) / 2)
            .attr("y", e => leftYFunction(e) - 5)
            .attr('text-anchor', 'middle')
            // .attr('dominant-baseline', 'middle');
            .attr('font-size', '10px')
            .attr('font-family', 'sans-serif')
            .text(e => Math.round(e.average * 100) / 100);

        // Right data
        if(utils.isDefined(this.rightSeries)) {
            let color = d3.rgb(0, 0, 255, 0.5);
            var rightData = group === null ? this.rightSeries.data : this.rightSeries[group]();
            dataGroup.selectAll('line').data(rightData).enter().append('line')
                .attr('x1', e => scales.bottom(e.first))
                .attr('y1', e => scales.right(e.average))
                .attr('x2', e => scales.bottom(e.last.clone().endOf('day')))
                .attr('y2', e => scales.right(e.average))
                .attr('stroke-width', 2)
                .attr('stroke', color);
        }

        return dataGroup.node();
    }
}