import * as d3 from 'd3';

import {vertex, cumulativeEdge, series} from "./es6series.js";
import {MultiGraph} from './graph.js';
import {isDefined} from './utils.js';

$(() => {
    $('#group').change(e => {
        let group = $(e.target).val();
        group = group == '' ? null : group;

        // var meterstanden = './kattenberg.json';
        var meterstanden = './data/steenbakkerijen.json';
        Promise.all([
            series('Outside temperature', '°C', 'data/temperaturen.json', vertex),
            series('Natural gas', 'kWh', meterstanden, cumulativeEdge, d => d.gas ? d.gas * 9.8507 : null), // 9.8507 kWh per m³
            series('Water', 'L', meterstanden, cumulativeEdge, d => d.water ? d.water * 1000 : null), // 1000 liters water per m³
            series('Electricity', 'kWh', meterstanden, cumulativeEdge, d => d.electricity ? d.electricity.day + d.electricity.night : null)
        ]).then((series) => {
            print(series, group);
        });
    }).change();
});

function print(series, group) {
    // display(series[1], series[0], 'weeks', 'experiment');
    // display(series[1], null, 'weeks', 'experiment');
    display(series[1], null, group, 'gas');
    display(series[2], null, group, 'water');
    display(series[3], null, group, 'electricity');
    display(series[0], null, group, 'temperature');
}

let toString = datum => {
    console.log(`range: [${datum.first.format('YYYY-MM-DD dd')}, ${datum.last.format('YYYY-MM-DD dd')}], duration: ${datum.duration}d, average: ${datum.average}`);
};

function display(leftSeries, rightSeries, group, targetElementId) {
    var targetElement = d3.select('#' + targetElementId);
    targetElement.html("");
    targetElement.append('h1').attr('style', 'float: left;').node().innerHTML = `${leftSeries.name} <small>(average ${leftSeries.unit} per day)</small>`;
    if(isDefined(rightSeries)) {
        targetElement.append('h1').attr('style', 'float: right;').node().innerHTML = `${rightSeries.name} <small>(${rightSeries.unit})</small>`;
    }
    let scrollPane = targetElement.append('div').attr('style', 'width: 100%; overflow-x: auto');
    new MultiGraph(leftSeries, rightSeries).draw(scrollPane, 2, group);
    scrollPane.node().scrollLeft = scrollPane.node().scrollWidth;
}