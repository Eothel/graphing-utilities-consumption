module.exports = function(config) {
    config.set({
        frameworks: ['mocha'],

        files: [
            'test/**/*.js'
        ],

        preprocessors: {
            'test/**/*.js': ['webpack']
        },

        webpack: {},

        reporters: ["mocha"],

        browsers: ['Firefox']
    })
};
