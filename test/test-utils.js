const assert = require('chai').assert;

const utils = require('../src/utils.js');

describe('utils', () => {
    describe('isNumber()', () => {
        it('return true if passed a number', () => {
            assert.isTrue(utils.isNumber(1507));
            assert.isTrue(utils.isNumber(1507.83));
        });

        it('return false if passed not passed a number', () => {
            assert.isFalse(utils.isNumber("1507"));
            assert.isFalse(utils.isNumber("hello world"));
            assert.isFalse(utils.isNumber([1, 2, 3]));
            assert.isFalse(utils.isNumber({name: 'Jimi'}));
            assert.isFalse(utils.isNumber(function() {}));
        });
    });

    describe('isNull()', () => {
        it('returns true if value is null', () => {
            assert.isTrue(utils.isNull(null));
        });

        it('returns false if value is not null', () => {
            assert.isFalse(utils.isNull("i am not null"));
        });
    });

    describe('isUndefined()', () => {
        it('returns true if value is undefined', () => {
            assert.isTrue(utils.isUndefined(undefined));
        });

        it('returns false if value is defined', () => {
            assert.isFalse(utils.isUndefined("i am defined"));
        });
    });

    it('isFunction');
    it('isString');
    it('isDefined');
    it('requiredString');
    it('requiredFunction');

    describe('round()', () => {
        it('value is mandatory', () => {
            assert.throw(() => {
                utils.round();
            }, `Parameter 'value' must be neither null nor undefined`);
        });

        it('value must be a number', () => {
            assert.throw(() => {
                utils.round("hello");
            }, `Parameter 'value' must be of type number`);
        });

        it('precision is mandatory', () => {
            assert.throw(() => {
                utils.round(15.07);
            }, `Parameter 'precision' must be neither null nor undefined`);
        });

        it('precision must be a number', () => {
            assert.throw(() => {
                utils.round(15.07, "hello");
            }, `Parameter 'precision' must be of type number`);
        });

        it('rounds up when next digit is >= 5', () => {
            assert.equal(15.08, utils.round(15.0783, 2));
        });

        it('rounds down when next digit is < 5', () => {
            assert.equal(15.078, utils.round(15.0783, 3));
        });
    });

    describe('required()', () => {
        it('throws error when value is null or undefined', () => {
            assert.throws(() => {
                utils.required(null, "myParamName");
            }, `Parameter 'myParamName' must be neither null nor undefined`);
            assert.throws(() => {
                utils.required(undefined, "myParamName");
            }, `Parameter 'myParamName' must be neither null nor undefined`);
        });

        it('passes if value is neither null nor undefined', () => {
            assert.equal(1507, utils.required(1507, "myParamName"));
            assert.equal("hello", utils.required("hello", "myParamName"));
        });
    });

    describe('requiredNumber()', () => {
        it('throws error when value is not a number', () => {
            assert.throw(() => {
                utils.requiredNumber("hello", "myParamName");
            }, `Parameter 'myParamName' must be of type number`);
            assert.throw(() => {
                utils.requiredNumber("1507", "myParamName");
            }, `Parameter 'myParamName' must be of type number`);
        });

        it('passes when value is a number', () => {
            assert.equal(1507, utils.requiredNumber(1507, "myParamName"));
            assert.equal(1507.83, utils.requiredNumber(1507.83, "myParamName"));
        });
    });

    describe('requiredType()', () => {
        it('throws error if value is not of given type', () => {
            assert.throw(() => {
                utils.requiredType("hello", Date, "myParamName");
            }, `Parameter 'myParamName' must be of type Date`);
            assert.throw(() => {
                utils.requiredType(new Date(), Promise, "myParamName");
            }, `Parameter 'myParamName' must be of type Promise`);
        });

        it('passes if value is of given type', () => {
            let value = new Date('1983-07-15');
            assert.equal(value, utils.requiredType(value, Date, "myParamName"))
        });
    });

    describe('requiredArray()', () => {
        it('throws error when value is not an array', () => {
            assert.throw(() => {
                utils.requiredArray("hello", "myParamName");
            }, `Parameter 'myParamName' must be of type Array`);
            assert.throw(() => {
                utils.requiredArray(new Date(), "myParamName");
            }, `Parameter 'myParamName' must be of type Array`);
        });

        it('passes if value is an array', () => {
            assert.deepEqual([1, 2, 3], utils.requiredArray([1, 2, 3], 'myParamName'));
        });
    });
});